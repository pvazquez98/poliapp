package com.example.apppolicia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private static final int REQUEST_CAMERA = 32;

    private Button iniciar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Rutina que llena la base de datos, no es necesario volver a ejecutar a menos que la bd se actualice
        try {
            DBHelper helper = new DBHelper(this, "policia", null, 1);
            SQLiteDatabase database = helper.getWritableDatabase();

            if(database != null) {
                database.execSQL("INSERT INTO question VALUES(1 , '¿Qué hacia usted en ese lugar?')");
                database.execSQL("INSERT INTO question VALUES(2 , '¿Cómo comenzó?')");
                database.execSQL("INSERT INTO question VALUES(3 , '¿Quiénes fueron participes del acontecimiento?')");
                database.execSQL("INSERT INTO question VALUES(4 , '¿Donde se encontraba?')");
                database.execSQL("INSERT INTO question VALUES(5 , '¿Cuál es su nombre?')");
                database.execSQL("INSERT INTO question VALUES(6 , '¿Qué ocurrio?')");
                database.execSQL("INSERT INTO question VALUES(7 , '¿Usted estaba acompañada?')");
                database.execSQL("INSERT INTO question VALUES(8 , '¿Para qué fue a ese lugar?')");
                database.execSQL("INSERT INTO question VALUES(9 , '¿Qué edad tiene?')");
                database.execSQL("INSERT INTO question VALUES(10 , '¿Recuerda usted si habia algien más presente?')");
            }
        }
        catch (Exception e) {
            Log.e("err", e.toString());
        }

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Explicamos porque necesitamos el permiso
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.CAMERA)) {

            }
            else {

                // El usuario no necesitas explicación, puedes solicitar el permiso:
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.CAMERA},
                        REQUEST_CAMERA);

                //
            }
        }

        iniciar = findViewById(R.id.iniciar);

//        Metodo para lanzar activity de escaner
        iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplication(), Affdex.class);
                startActivity(intent);
            }
        });
    }
}
